package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-co-op/gocron"
	"io"
	"kimsufi-notify/internal/config"
	"log"
	"net/http"
	"time"
)

type ServerAvailability struct {
	Region      string `json:"region"`
	Datacenters []struct {
		Datacenter   string `json:"datacenter"`
		Availability string `json:"availability"`
	} `json:"datacenters"`
	Hardware string `json:"hardware"`
}

type DiscordWebhook struct {
	Content string `json:"content"`
}

var previousData = make(map[string]string)

func main() {
	var scheduler = gocron.NewScheduler(time.UTC)
	scheduler.TagsUnique()

	log.Println("Scheduling check", config.Schedule())
	log.Println("Filtering for region", config.Region())
	log.Println("Filtering for hardwares", config.Hardwares())
	_, err := scheduler.Cron(config.Schedule()).StartImmediately().Do(func() {
		defer func() {
			if r := recover(); r != nil {
				log.Println("Recovered", r)
			}
		}()

		queryServerAvailability()
	})
	if err != nil {
		return
	}

	scheduler.StartBlocking()
}

func queryServerAvailability() {
	resp, err := http.Get("https://www.ovh.com/engine/api/dedicated/server/availabilities?country=fr")
	if err != nil {
		panic(err)
	}

	if resp.StatusCode != 200 {
		panic(errors.New("bad response from server"))
	}

	defer resp.Body.Close()
	byt, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	var availabilities []ServerAvailability
	if err := json.Unmarshal(byt, &availabilities); err != nil {
		panic(err)
	}
	var hardwareMap = make(map[string]bool, len(config.Hardwares()))
	for _, h := range config.Hardwares() {
		hardwareMap[h] = true
	}

	var ln int
	for _, av := range availabilities {
		if av.Region != config.Region() {
			continue
		}

		if v, exist := hardwareMap[av.Hardware]; !exist || !v {
			continue
		}

		for _, dc := range av.Datacenters {
			k := fmt.Sprintf("%s-%s", av.Hardware, dc.Datacenter)
			if previous, exist := previousData[k]; !exist || previous != dc.Availability {
				previousData[k] = dc.Availability
				go postAvailabilityUpdate(av.Hardware, av.Region, dc.Datacenter, dc.Availability)
			}

		}
		availabilities[ln] = av
		ln++
	}

	availabilities = availabilities[:ln]
}

func postAvailabilityUpdate(hardware string, region string, datacenter string, availability string) {
	msg := fmt.Sprintf("Availability for %s in %s - %s changed to %s", hardware, region, datacenter, availability)
	log.Println(msg)
	data := DiscordWebhook{
		Content: msg,
	}
	byt, err := json.Marshal(data)
	if err != nil {
		panic(err)
	}

	_, err = http.Post(config.DiscordWebhookUrl(), "application/json; charset=UTF-8", bytes.NewBuffer(byt))
	if err != nil {
		panic(err)
	}
}
