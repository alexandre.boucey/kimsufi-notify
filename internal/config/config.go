package config

import (
	"github.com/spf13/viper"
)

func init() {
	viper.SetDefault("schedule", "*/5 * * * *")
	viper.SetDefault("region", "europe")

	viper.SetEnvPrefix("ksnot")
	viper.AutomaticEnv()
}

func Schedule() string {
	return viper.GetString("schedule")
}

func Region() string {
	return viper.GetString("region")
}

func Hardwares() []string {
	return viper.GetStringSlice("hardwares")
}

func DiscordWebhookUrl() string {
	return viper.GetString("discord_webhook_url")
}