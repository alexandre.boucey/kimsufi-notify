FROM golang:1.17-alpine AS builder
RUN apk update && apk add --no-cache ca-certificates tzdata && update-ca-certificates

# Create appuser
# See https://stackoverflow.com/a/55757473/12429735RUN
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "10001" \
    "appuser"

WORKDIR /build
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -v -ldflags='-s -w -extldflags "-static"' -a -tags netgo -o app ./cmd/kimsufi-notify

FROM scratch
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /build/app /app

USER appuser:appuser

CMD ["/app"]